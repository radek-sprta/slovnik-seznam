#!/usr/bin/python3

import argparse
import logging

try:
    from urllib import urlencode
    from urllib2 import urlopen, URLError
except ImportError:
    from urllib.request import urlopen  # pylint: disable=E0611,F0401
    from urllib.parse import urlencode  # pylint: disable=E0611,F0401
    from urllib.error import URLError  # pylint: disable=E0611,F0401

from bs4 import BeautifulSoup


LANGUAGES = {"cz", "de", "en", "es", "fr", "it", "ru", "sk"}


def get_results_page(word, fromLanguage, toLanguage):
    baseUrl = "https://slovnik.seznam.cz"
    query = urlencode({"q": word})
    url = "%s/%s-%s/word/?%s" % (baseUrl, fromLanguage, toLanguage, query)
    try:
        with urlopen(url) as page:
            return page.read()
    except URLError as e:
        raise URLError("url {} cannot be opened".format(url)) from e


def parse_args():
    parser = argparse.ArgumentParser(
        description="Jednoduchý skript který z výstupu webu https://slovnik.seznam.cz vrací jednoduchý textový výstup."
    )
    parser.add_argument(
        "--from", "-f", choices=LANGUAGES, default="cz", help="Zdrojový jazyk"
    )
    parser.add_argument(
        "--to", "-t", choices=LANGUAGES, default="en", help="Výstupní jazyk"
    )
    parser.add_argument("word", help="Překládané slovo")
    return parser.parse_args()


def parse_meaning(raw):
    for br in raw.find_all("br"):
        br.replace_with("$")
    for meaning in raw.text.split("$"):
        yield meaning.strip("\n").replace("\n", " ").replace(" , ", ", ")


def preview(word, fromLanguage, toLanguage):
    if fromLanguage == toLanguage:
        yield word
    else:
        try:
            result = get_results_page(word, fromLanguage, toLanguage)
        except URLError as e:
            logging.error(e)
        else:
            bs = BeautifulSoup(result, "lxml")
            for meaning in bs.select("div#fastMeanings td"):
                for translation in parse_meaning(meaning):
                    if translation:
                        yield translation


def main():
    args = vars(parse_args())
    if "cz" not in (args["to"], args["from"]):
        logging.error("Alespoň jeden z jazyků musí být čeština.")
        exit(1)

    for result in preview(args["word"], args["from"], args["to"]):
        print(result)


if __name__ == "__main__":
    main()
